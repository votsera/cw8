﻿using AutoMapper;
using CW8.DAL.Entities;
using CW8.UI.Models.Topic;

namespace CW8.UI
{
    public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateUniversalMap<Topic, TopicIndexModel>();
            }
            
            private void CreateUniversalMap<From, To>()
            {
                CreateMap<From, To>();
                CreateMap<To, From>();
            }
        }
    

}