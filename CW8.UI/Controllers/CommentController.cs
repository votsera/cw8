﻿using System;
using System.Threading.Tasks;
using CW8.DAL.Entities;
using CW8.UI.Models.Topic;
using CW8.UI.Services.Comment.Contracts;
using CW8.UI.Services.Topic.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CW8.UI.Controllers
{
    public class CommentController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly ICommentService _commentService;

        public CommentController(UserManager<User> userManager,ICommentService commentService)
        {
            if(commentService == null)
                throw new ArgumentNullException(nameof(commentService));
            _userManager = userManager;
            _commentService = commentService;
        }

        
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Add(int topicId, string commentText)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _commentService.Add(topicId, currentUser, commentText);
        
                return RedirectToAction("Show", "Topic", topicId);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        
    }
}