﻿using System;
using PagedList;
using System.Threading.Tasks;
using CW8.DAL.Entities;
using CW8.UI.Models.Topic;
using CW8.UI.Services.Topic.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CW8.UI.Controllers
{
    public class TopicController: Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly ITopicService _topicService;

        public TopicController(UserManager<User> userManager,ITopicService topicService)
        {
            if(topicService == null)
                throw new ArgumentNullException(nameof(topicService));
            _userManager = userManager;
            _topicService = topicService;
        }
        
        [HttpGet("Forum")]
        public IActionResult Index( int? page)
        {
            try
            {
                var topics = _topicService.GetTopicsList();
                int pageSize = 3;
                int pageNumber = (page ?? 1);
                return View(topics.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        public IActionResult Create()
        {
            var model = _topicService.GetTopicCreateModel();

            return View(model);
        }
        
        public IActionResult Show(int topicId)
        {
            var model = _topicService.GetTopicById(topicId);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Create(TopicCreateModel model )
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _topicService.CreateTopic(model, currentUser);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        
    }
}