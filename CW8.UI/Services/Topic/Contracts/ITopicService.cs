﻿using System.Collections.Generic;
using CW8.DAL.Entities;
using CW8.UI.Models.Topic;

namespace CW8.UI.Services.Topic.Contracts
{
    public interface ITopicService
    {
        public void CreateTopic(TopicCreateModel model, User user);

        public TopicCreateModel GetTopicCreateModel();

        public List<DAL.Entities.Topic> GetTopicsList();
        DAL.Entities.Topic GetTopicById(int topicId);
    }
}