﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CW8.DAL;
using CW8.DAL.Entities;
using CW8.UI.Models.Topic;
using CW8.UI.Services.Topic.Contracts;
using Microsoft.AspNetCore.Identity;

namespace CW8.UI.Services.Topic
{
    public class TopicService: ITopicService
    {
        private readonly UserManager<User> _userManager;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;


        public TopicService(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
        }

        public void CreateTopic(TopicCreateModel model, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                try
                {
                    var topic = new DAL.Entities.Topic(){
                        Title = model.Title, 
                        Description = model.Title, 
                        CreatedOn = DateTime.Now,
                        TopicStarterId = user.Id
                    };
                    unitOfWork.Topics.Create(topic);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                
            }
        }

        public TopicCreateModel GetTopicCreateModel()
        {
            return new TopicCreateModel();
        }

        public List<DAL.Entities.Topic> GetTopicsList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                try
                {
                    return unitOfWork.Topics.GetAllWithTopicStartersSortedByDate().ToList();
                    
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                
            }
        }

        public DAL.Entities.Topic GetTopicById(int topicId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                try
                {
                    var topic = unitOfWork.Topics.
                        GetByIdWithWithTopicStarterAndComments(topicId);
                    return topic;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
                
            }
        }
        
    }
}