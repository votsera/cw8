﻿using CW8.DAL.Entities;

namespace CW8.UI.Services.Comment.Contracts
{
    public interface ICommentService
    {
        void Add(in int topicId, User currentUser, string commentText);
    }
}