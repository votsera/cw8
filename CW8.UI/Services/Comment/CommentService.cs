﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CW8.DAL;
using CW8.DAL.Entities;
using CW8.UI.Models.Topic;
using CW8.UI.Services.Topic.Contracts;
using Microsoft.AspNetCore.Identity;
using CW8.UI.Services.Comment.Contracts;

namespace CW8.UI.Services.Comment
{
    public class CommentService : ICommentService
    {
        private readonly UserManager<User> _userManager;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;


        public CommentService(IUnitOfWorkFactory unitOfWorkFactory, UserManager<User> userManager)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
            _userManager = userManager;
        }

        public void Add(in int topicId, User user, string commentText)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                try
                {
                    var comment = new DAL.Entities.Comment()
                    {
                        TopicId = topicId,
                        CommentText = commentText,
                        CreatedOn = DateTime.Now,
                        CommentAuthorId = user.Id
                    };
                    unitOfWork.Comments.Create(comment);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }
    }
}