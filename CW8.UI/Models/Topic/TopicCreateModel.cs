﻿using System.ComponentModel.DataAnnotations;

namespace CW8.UI.Models.Topic
{
    public class TopicCreateModel
    {
        [Display(Name = "Заголовок")]
        [Required(ErrorMessage = "Без заголовка нельзя")]
        public string Title { get; set; }

        [Display(Name = "Ваш текст")]
        [Required(ErrorMessage = "Без содержимого скучно")]
        public string Description { get; set; }
    }
}