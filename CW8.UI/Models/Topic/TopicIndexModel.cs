﻿using System;
using System.Collections.Generic;
using CW8.DAL.Entities;

namespace CW8.UI.Models.Topic
{
    public class TopicIndexModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string CreatedOn{ get; set; }
        public User TopicStarter { get; set; }
        public List<Comment> Comments { get; set; }
    }
}