﻿using System.Collections.Generic;
using CW8.DAL.Entities;

namespace CW8.DAL.Repositories.Contracts
{
    public interface ICommentRepository: IRepository<Comment>
    {
        IEnumerable<Comment> GetAllByAuthor(int id);
    }
}