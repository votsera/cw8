﻿using System.Collections.Generic;
using CW8.DAL.Entities;

namespace CW8.DAL.Repositories.Contracts
{
    public interface ITopicRepository: IRepository<Topic>
    {
        IEnumerable<Topic> GetAllWithTopicStartersSortedByDate();
        Topic GetByIdWithWithTopicStarterAndComments(int id);
    }
}