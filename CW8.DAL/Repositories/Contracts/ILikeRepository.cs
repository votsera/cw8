﻿using System.Collections.Generic;
using CW8.DAL.Entities;

namespace CW8.DAL.Repositories.Contracts
{
    public interface ILikeRepository: IRepository<Like>
    {
        IEnumerable<Like> GetAllWithCommentOwnersByComment(int id);
    }
}