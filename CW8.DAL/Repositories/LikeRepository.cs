﻿using System.Collections.Generic;
using System.Linq;
using CW8.DAL.Entities;
using CW8.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace CW8.DAL.Repositories
{
    public class LikeRepository: Repository<Like>, ILikeRepository
    {
        public LikeRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Likes;
        }

        public IEnumerable<Like> GetAllWithCommentOwnersByComment(int id)
        {
            return entities
                .Include(e=> e.Comment)
                .Include(e => e.LikeOwner)
                .Where(t=>t.LikeOwnerId == id)
                .ToList();
        }
    }
}