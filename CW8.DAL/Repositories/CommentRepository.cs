﻿using System.Collections.Generic;
using System.Linq;
using CW8.DAL.Entities;
using CW8.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace CW8.DAL.Repositories
{
    public class CommentRepository: Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Comments;
        }

        public IEnumerable<Comment> GetAllByAuthor(int id)
        {
            return entities
                .Include(e => e.CommentAuthor)
                .Include(e => e.Likes)
                .ToList();
        }
    }
}