﻿using System.Collections.Generic;
using System.Linq;
using CW8.DAL.Entities;
using CW8.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace CW8.DAL.Repositories
{
    public class TopicRepository: Repository<Topic>, ITopicRepository
    {
        public TopicRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Topics;
        }


        public IEnumerable<Topic> GetAllWithTopicStartersSortedByDate()
        {
            return entities.OrderBy(e=> e.CreatedOn)
                .Include(e => e.TopicStarter)
                .Include(e => e.Comments)
                .ToList();
        }

        public Topic GetByIdWithWithTopicStarterAndComments(int id)
        {
            return entities
                .Include(e => e.TopicStarter)
                .Include(e => e.Comments).First(t=>t.Id == id);
        }
    }
}