﻿using System;
using CW8.DAL.Repositories;
using CW8.DAL.Repositories.Contracts;

namespace CW8.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public ITopicRepository Topics { get; }
        public ICommentRepository Comments { get; }
        public ILikeRepository Likes { get; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Topics = new TopicRepository(context);
            Likes = new LikeRepository(context);
            Comments = new CommentRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if(_disposed)
                return;

            if(disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}