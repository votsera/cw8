﻿using System;
using CW8.DAL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CW8.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : Entity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}