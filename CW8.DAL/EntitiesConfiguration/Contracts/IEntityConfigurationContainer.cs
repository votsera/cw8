﻿using CW8.DAL.Entities;

namespace CW8.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Topic> TopicConfiguration { get; }
        IEntityConfiguration<Comment> CommentConfiguration { get; }
        IEntityConfiguration<Like> LikeConfiguration { get; }
    }
}