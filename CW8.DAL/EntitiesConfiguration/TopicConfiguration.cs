﻿using CW8.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CW8.DAL.EntitiesConfiguration
{
    public class TopicConfiguration: BaseEntityConfiguration<Topic>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Topic> builder)
        {
            builder.Property(p => p.Title)
                .HasMaxLength(50)
                .IsRequired();
            builder.Property(p => p.Description)
                .HasMaxLength(1000)
                .IsRequired();
        }
        
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Topic> builder)
        {
            builder
                .HasOne(t => t.TopicStarter)
                .WithMany(u => u.Topics)
                .HasForeignKey(t => t.TopicStarterId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasMany(t => t.Comments)
                .WithOne(c => c.Topic)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(t => t.TopicId);
        }
    }
}