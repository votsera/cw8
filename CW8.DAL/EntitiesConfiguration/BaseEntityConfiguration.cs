﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using CW8.DAL.Entities;
using CW8.DAL.EntitiesConfiguration.Contracts;

namespace CW8.DAL.EntitiesConfiguration
{
    public abstract class BaseEntityConfiguration<T> : IEntityConfiguration<T> where T : Entity
    {
        public Action<EntityTypeBuilder<T>> ProvideConfigurationAction()
        {
            return ConfigureEntity;
        }

        private void ConfigureEntity(EntityTypeBuilder<T> builder)
        {
            ConfigureProperties(builder);
            ConfigurePrimaryKeys(builder);
            ConfigureForeignKeys(builder);
        }

        protected virtual void ConfigureProperties(EntityTypeBuilder<T> builder) { }

        protected virtual void ConfigurePrimaryKeys(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(e => e.Id);
        }

        protected virtual void ConfigureForeignKeys(EntityTypeBuilder<T> builder) { }

    }
}