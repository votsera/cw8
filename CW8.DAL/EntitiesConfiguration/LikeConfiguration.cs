﻿using CW8.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CW8.DAL.EntitiesConfiguration
{
    public class LikeConfiguration: BaseEntityConfiguration<Like>
    {
        protected override void ConfigurePrimaryKeys(EntityTypeBuilder<Like> builder)
        {
            builder
                .HasOne(l => l.Comment)
                .WithMany(c => c.Likes)
                .HasForeignKey(l => l.CommentId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasOne(l => l.LikeOwner)
                .WithMany(u => u.Likes)
                .HasForeignKey(l => l.LikeOwnerId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}