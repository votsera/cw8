﻿using CW8.DAL.Entities;
using CW8.DAL.EntitiesConfiguration.Contracts;

namespace CW8.DAL.EntitiesConfiguration
{
    public class EntityConfigurationContainer: IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Topic> TopicConfiguration { get; }
        public IEntityConfiguration<Comment> CommentConfiguration { get; }
        public IEntityConfiguration<Like> LikeConfiguration { get; }

        public EntityConfigurationContainer()
        {
            TopicConfiguration = new TopicConfiguration();
            CommentConfiguration = new CommentConfiguration();
            LikeConfiguration = new LikeConfiguration();
        }
    }
}