﻿using CW8.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CW8.DAL.EntitiesConfiguration
{
    public class CommentConfiguration: BaseEntityConfiguration<Comment>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Comment> builder)
        {
            
            builder.Property(p => p.CommentText)
                .HasMaxLength(1000)
                .IsRequired();
        }
        
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Comment> builder)
        {
            builder
                .HasOne(c => c.CommentAuthor)
                .WithMany(u => u.Comments)
                .HasForeignKey(c => c.CommentAuthorId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasMany(c => c.Likes)
                .WithOne(l => l.Comment)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(l => l.CommentId);
        }
    }
}