﻿using System;
using System.Collections.Generic;

namespace CW8.DAL.Entities
{
    public class Topic: Entity
    {
        public int TopicStarterId { get; set; }
        public User TopicStarter { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}