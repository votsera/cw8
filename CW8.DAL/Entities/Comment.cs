﻿using System;
using System.Collections.Generic;

namespace CW8.DAL.Entities
{
    public class Comment: Entity
    {
        public int CommentAuthorId { get; set; }
        public User CommentAuthor { get; set; }
        public int TopicId { get; set; }
        public Topic Topic { get; set; }
        public DateTime CreatedOn { get; set; }
        public string CommentText { get; set; }
        public ICollection<Like> Likes { get; set; }
    }
}