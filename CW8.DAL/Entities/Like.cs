﻿namespace CW8.DAL.Entities
{
    public class Like: Entity
    {
        public int LikeOwnerId { get; set; }
        public User LikeOwner { get; set; }
        public int CommentId { get; set; }
        public Comment Comment { get; set; }
    }
}