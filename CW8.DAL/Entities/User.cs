﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace CW8.DAL.Entities
{
    public class User : IdentityUser<int>
    {
        public ICollection<Topic> Topics { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Like> Likes { get; set; }
    }
}