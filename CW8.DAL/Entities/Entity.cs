﻿namespace CW8.DAL.Entities
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}